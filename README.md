# Spring Framework Examples #

This repository will contain examples for different modules of the Spring Framework ecosystem.

### How do I get set up? ###

* You can use Git to fetch the source code and build it using Maven
* If you are using an IDE (such as Eclipse or IntelliJ), you should be able to run each example easily