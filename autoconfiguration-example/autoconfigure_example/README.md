# Auto Configuration Example #

In this example, you can see how you can create your own autoconfiguration files for your own Spring Beans.

### Understanding the example ###

For this example, we are using a Spring Boot Starter class (Application) that is bootstraping the Spring context. It will autoload the `@Configuration` annotated classes under its package (onepackage).

So by default, all the beans instantiated in the ExtraConfiguration class will be instantiated and used. You can see this in the [Application class](https://bitbucket.org/omikronic/spring-examples/src/0195f94914bb2ccbdb0b1e2354a256302296b861/autoconfiguration-example/autoconfigure_example/src/main/java/onepackage/Application.java?at=master&fileviewer=file-view-default#Application.java-18).

But the beans instantiated in a different packages are ignored. In order to enable your Auto Configuration classes in Spring Boot, you must create a [META-INF/spring.factories](https://bitbucket.org/omikronic/spring-examples/src/0195f94914bb2ccbdb0b1e2354a256302296b861/autoconfiguration-example/autoconfigure_example/src/main/resources/META-INF/spring.factories?at=master&fileviewer=file-view-default) file and include there a property with the name **org.springframework.boot.autoconfigure.EnableAutoConfiguration** and a list of your Spring configuration classes separated by comma (**,**).

In this example, that property is commented to confirm that those beans are not being instantiated by Spring until you uncomment it and confirm that it starts to work.



Enjoy it.