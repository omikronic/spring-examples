package otherpackage.logic;

public class OtherImplementation implements OtherInterface {

	@Override
	public void doSomethingElse() {
		System.out.println("##### Doing something else");
	}

}
