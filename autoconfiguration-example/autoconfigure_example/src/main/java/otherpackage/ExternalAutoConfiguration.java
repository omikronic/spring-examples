package otherpackage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import otherpackage.logic.OtherImplementation;
import otherpackage.logic.OtherInterface;

@Configuration
class ExternalAutoConfiguration {
	
	@Bean
	OtherInterface other() {
		return new OtherImplementation();
	}
}