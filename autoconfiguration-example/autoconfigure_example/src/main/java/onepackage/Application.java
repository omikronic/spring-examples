package onepackage;

import java.util.Map;

import onepackage.logic.OneInterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import otherpackage.logic.OtherInterface;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class);
		OneInterface one = context.getBean(OneInterface.class);
		one.doSomething();
		Map<String, OtherInterface> beans = context.getBeansOfType(OtherInterface.class);
		if (!beans.isEmpty()) {
			OtherInterface other = context.getBean(OtherInterface.class);
			other.doSomethingElse();
		}
		else {
			System.out.println("Remember to enable de auto configuration uncommenting the line in the META-INF/spring.factories file");
		}
	}
}