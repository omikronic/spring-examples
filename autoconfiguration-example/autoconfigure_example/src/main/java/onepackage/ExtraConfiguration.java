package onepackage;

import onepackage.logic.OneImplementation;
import onepackage.logic.OneInterface;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ExtraConfiguration {
	
	@Bean
	OneInterface one() {
		return new OneImplementation();
	}
}